import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Flutter Demo',
    theme: ThemeData(
      primarySwatch: Colors.green,
    ),
    home: const UserPage(),
  ));
}

class UserPage extends StatefulWidget {
  const UserPage({Key? key}) : super(key: key);

  @override
  State<UserPage> createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  String? username, nationality;
  double? age;

  getUsername(username) {
    this.username = username;
    print(this.username);
  }

  getNationality(nationality) {
    this.nationality = nationality;
    print(this.nationality);
  }

  getAge(age) {
    this.age = double.parse(age);
    print(this.age);
  }

  createData() {
    DocumentReference documentReference =
        FirebaseFirestore.instance.collection('users').doc(username);
    Map<String, dynamic> info = {
      'username': username,
      'nationality': nationality,
      'age': age,
    };

    documentReference.set(info).whenComplete(() {
      print('$username created');
    });
  }

  readData() {
    DocumentReference documentReference =
        FirebaseFirestore.instance.collection('users').doc(username);

    documentReference.get().then((datasnapshot) {
      print(datasnapshot.data());
      print(datasnapshot.data());
      print(datasnapshot.data());
    });
  }

  updateData() {
    DocumentReference documentReference =
        FirebaseFirestore.instance.collection('users').doc(username);
    Map<String, dynamic> info = {
      'username': username,
      'nationality': nationality,
      'age': age,
    };

    documentReference.set(username).whenComplete(() {
      print('$username updated');
    });
  }

  deleteData() {
    DocumentReference documentReference =
        FirebaseFirestore.instance.collection('users').doc(username);

    documentReference.delete().whenComplete(() => print('$username deleted'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Firebase CRUD'),
      ),
      body: Column(
        children: [
          SizedBox(
            height: 1.5,
            width: 1.5,
            child: Stack(
              fit: StackFit.expand,
              clipBehavior: Clip.none,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(3.0),
            child: TextField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Username',
              ),
              onChanged: (String username) {
                getUsername(username);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(3.0),
            child: TextField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Nationality',
              ),
              onChanged: (String nationality) {
                getNationality(nationality);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(3.0),
            child: TextField(
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Age',
              ),
              onChanged: (String age) {
                getAge(age);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(
              textDirection: TextDirection.ltr,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 5.0),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.green, onPrimary: Colors.black),
                      onPressed: () {
                        createData();
                      },
                      child: const Text('Create')),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 5.0),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.blue, onPrimary: Colors.black),
                      onPressed: () {
                        readData();
                      },
                      child: const Text('Read')),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 5.0),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.yellow, onPrimary: Colors.black),
                      onPressed: () {
                        updateData();
                      },
                      child: const Text('Update')),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 5.0),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.red, onPrimary: Colors.black),
                      onPressed: () {
                        deleteData();
                      },
                      child: const Text('Delete')),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
